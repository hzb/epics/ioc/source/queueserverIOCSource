# Import the basic framework components.
# Import the basic framework components.
from cothread.catools import caget, connect, camonitor,caput
from softioc import softioc, builder
import cothread

#synchronous communication with the qserver
from bluesky_queueserver_api.zmq import REManagerAPI

from bluesky_queueserver.manager.qserver_cli import create_msg, zmq_single_request, QServerExitCodes
from bluesky_queueserver import ZMQCommSendThreads, CommTimeoutError, bind_plan_arguments, ReceiveConsoleOutput
import os
from datetime import datetime, timedelta
import pytz
from softioc.builder import records
import pickle
import json
from os.path import exists

#Initialise Run Enging Manager Connection. Default is to localhost
RM = REManagerAPI()

#Read from the save file:
file_path = 'autosave.json'
if exists(file_path):
    with open(file_path) as json_file:
        autosave = json.load(json_file)
# Or make a new one if we don't have any
else:
    autosave = {}


#db = databroker.catalog["sissy2"]
from tiled.client import from_uri
from tiled.queries import Key
db = from_uri("http://172.17.9.22:8001/api")

address = 'tcp://127.0.0.1:60615'
BEAMLINE = "UE48"
END_STATIONS = ["SISSY1", "SISSY2", "CAT", "STXM"]
client = ZMQCommSendThreads(zmq_server_address=address)
user_group = 'admin'
num_results = 10


#send plan function
def send_and_start_restore():

    """
    TO DO 
    
    - Add a read of the allowed plans of devices and generate the required PV's to enable or disable
    - Either 
    """



   
    devices = []
    for device_name, device_pv in allowed_devices_pvs[END_STATIONS[int(end_station.get())]].items():
        
 
        if device_pv.get() == 1:

            devices.append(device_name)





    restore_status.set("Starting Restore")       
    dev_list_string = '["'+'", "'.join(devices) + '"]'
    print(dev_list_string)
    
    send_string = '{"name": "switch", "args": [], "kwargs": {"devices":'   + str(dev_list_string) + ',"end_station":"'+ END_STATIONS[int(end_station.get())]+'","uid":"'+str(selected_uid.get()) +'"}}'
    method, params, monitoring_mode = create_msg(['queue', 'execute', 'plan', send_string], lock_key=None)
    response = client.send_message(method=method, params=params)

    if response['success']:
    
        uid = response['item']['item_uid']

        #Now let's track the plan to find out if it executes    if response['success'] == True:

        restore_uid.set(str(uid))
    else:

        restore_status.set(str(response['msg']))



def uid_in_queue(uid):
    
    """
    returns true or false
    """
    in_queue = False
    response = RM.queue_get()
    
    
    for item in response['items']:
        
        if item['item_uid'] == uid:
            in_queue = True
            return in_queue
        
    return in_queue

def uid_in_RE(uid):
    
    
    """
    returns true or false
    """
    
    in_RE = False
    
    response = RM.status()
    
    if response['running_item_uid'] == uid:
        in_RE = True
        return in_RE
    
    return in_RE
        
def uid_in_history(uid):
    
    """
    returns bool and status
    """
    in_history = False
    status = ""
    traceback = ""
    
    response = RM.history_get()
    
   
    for item in response['items']:
        if item['item_uid'] == uid:
            
            in_history = True
            status = item['result']['exit_status']
            traceback = item['result']['traceback']

            return in_history, status, traceback
    
    return in_history, status, traceback

def uid_status(uid):

    """
    return a string with info about where the plan is
    """

    if uid_in_queue(uid ):
        return "plan in queue"
    
    elif uid_in_RE(uid):
        return "plan running"

    in_history, exit_status, traceback = uid_in_history(uid )
    if in_history and exit_status != "failed":
        return exit_status
    elif in_history:

        return traceback
    
    return "not found"


def take_snapshot():

    

    is_expert =  expert.get() == 1

    if not is_expert:
        save_status.set("Saving...")
    else:
        save_status.set("Saving Expert...")

    


    send_string = '{"name": "snapshot", "args": [], "kwargs": {"md":{"snapshot_name":"' + str(snapshot_name.get()) + '","expert":"' + str(is_expert) +'"}}}'
    print(send_string)
    method, params, monitoring_mode = create_msg(['queue', 'execute', 'plan', send_string], lock_key=None)
    response = client.send_message(method=method, params=params)
    if response['success']:
    
        uid = response['item']['item_uid']

        #Now let's track the plan to find out if it executes    if response['success'] == True:

        save_uid.set(str(uid))
    else:

        save_status.set(str(response['msg']))

    
        
    
def update_selected_uid(value):

    #update the PV that stores the currently selected UID as a confirmation to the user of what they are about to do

    selected_uid_from_idx = search_results_uid[value].get()
    selected_uid.set(selected_uid_from_idx)
    selected_time.set(search_results_times[value].get())




        



def update_selected_baseline():

  
    print(int(end_station.get()))
    print(END_STATIONS[int(end_station.get())])
    print(f"expert config is {config_sel.get()==1} ")
    search_results = []
    if config_sel.get()==1:
        search_results = db.search(Key("beamline") == BEAMLINE).search(Key("end_station")== END_STATIONS[int(end_station.get())]).search(Key("expert")=='True')
        
    else:
        search_results = db.search(Key("beamline") == BEAMLINE).search(Key("end_station")== END_STATIONS[int(end_station.get())])
    print(f"num search results is {len(search_results)}")
    #For each result populate the results waveform so it can be displayed and selected later


    if len(search_results) < num_results:
        num = len(search_results)
    
    else:
        num = num_results

    for i in range(num_results):
        if i<len(search_results):
            run  = search_results[-i-1]
            metadata = run.metadata['start']
            start_time = metadata['time']
            tz = pytz.timezone("Europe/Berlin")
        
            start_time_str = str(datetime.fromtimestamp(start_time,tz).strftime('%Y-%m-%d %H:%M:%S'))
            print(start_time_str)
            search_results_times[i].set(start_time_str)
            uid = metadata['uid']
            search_results_uid[i].set(uid)

            info_string = metadata['plan_name'] + ",\t"

            if 'snapshot_name' in metadata:            

                info_string = info_string + metadata['snapshot_name'] + ",\t"

            if 'expert' in metadata:
                if metadata['expert'] == 'True':

                    info_string = "!!expert!!\t" + info_string

            

            search_results_info[i].set(info_string)
        else:
            search_results_uid[i].set("")
            search_results_times[i].set("")
            search_results_info[i].set("")
    #Assume we want the latest one
    selected_run_idx.set(0)


#Make all write PV's blocking
builder.SetBlocking(True)
# Set the record prefix
SYS = "EMILEL:QS:" 
DEV = "UE48"
PREFIX = SYS + DEV
builder.SetDeviceName(PREFIX)


# Create some records for RE
env_exists = builder.boolIn('ENVEXIST', initial_value = False, ZNAM="No RE Env",ONAM="RE Env Exists")
re_running = builder.boolIn('ENVRUNNING', initial_value = False, ZNAM="Idle",ONAM="Running")

#There is a probably a nicer way of doing this with the endstations list
end_station = builder.mbbOut('ENDSTATION', END_STATIONS[0], END_STATIONS[1],END_STATIONS[2],END_STATIONS[3], initial_value = 0)

config_sel = builder.mbbOut('CONFIGSEL','Last', 'Last Expert',initial_value = 0)
search = builder.Action('SEARCH', on_update=lambda v: update_selected_baseline() )

#Create records for the first 10 results

search_results_times = []
search_results_uid = []
search_results_info = []

for i in range(num_results):
    search_results_times.append(builder.longStringIn('RESTIMES'+str(i),length=1000, initial_value = "NA"))
    search_results_uid.append(builder.longStringIn('RESUID'+str(i),length=1000, initial_value = "NA"))
    search_results_info.append(builder.longStringIn('RESINFO'+str(i),length=1000, initial_value = "NA"))

selected_run_idx = builder.longOut('SELIDX',initial_value=0,always_update = True, on_update=lambda v: update_selected_uid(v))

selected_time = builder.stringIn('SELTIME', initial_value = "NA")
selected_uid = builder.stringIn('SELUID', initial_value = "NA")
extra_info = builder.stringIn('SELINFO', initial_value = "NA")


snapshot = builder.Action('SNAPSHOT', on_update=lambda v: take_snapshot() )
expert = builder.boolOut('EXPERT_SAVE', initial_value = False, ZNAM="Not Expert",ONAM="Expert")# A PV to determine whether the next save should be expert
save_uid = builder.stringIn('SAVEUID', initial_value = "")
save_status = builder.longStringIn('SAVESTATUS', initial_value = "",length=50012)
snapshot_name = builder.stringOut('SNAPSHOTNAME',initial_value = "")

# Records for restore
send_plan = builder.Action('STARTRESTORE', on_update=lambda v:send_and_start_restore() )
restore_uid = builder.stringIn('RESTOREUID', initial_value = "")
restore_status = builder.longStringIn('RESTORESTATUS', initial_value = "",length=50012)

# List of allowed devices
user_group = 'admin'

result = RM.devices_allowed(user_group="admin")
if result["success"] is False:
    raise RuntimeError(f"Failed to load list of allowed devices: {result['msg']}")

#Assumes that no positioner has positioner components
def walk(node, name, device_name, pos_list):
    
    #Check if the current node is a positioner 
    if 'is_positioner' in node and node['is_positioner']:
            pos_list.append(name)
            
    #If it isn't then see if it has any components which are
    
    for key, item in node.items():

        if isinstance(item, dict) :

            if key != 'components':
                #name = name + '.' + key #keep track of the name
                if 'is_positioner' in item :
                    if item['is_positioner']:
                        pos_list.append(name + '.' + key)
                        name = device_name
            else:
                walk(item,name, device_name, pos_list)

dict_of_devices={}
for key, item in result['devices_allowed'].items():
    list_of_positioner_components =[]
    walk(item, key,key, list_of_positioner_components)
    if len(list_of_positioner_components) >0:
        dict_of_devices[key]=list_of_positioner_components
    else:
        dict_of_devices[key]=None

allowed_devices_list_string ={}

for group_name in ['ADMIN',"SISSY1", "SISSY2", "CAT", "STXM"]:
    
    allowed_devices_list_string[group_name] = builder.longStringIn("DEVICES_LIST_"+ group_name, initial_value = "None", length=10000)




def update_allowed_devices_list(v,name):

    for group_name in ['ADMIN',"SISSY1", "SISSY2", "CAT", "STXM"]:
        devices =[]
        for device_name, device_pv in allowed_devices_pvs[group_name].items():
        
 
            if device_pv.get() == 1 and allowed_devices_pvs['ADMIN'][device_name].get() == 1:

                devices.append(device_name)

            #update the autosave parameters if it's changed

    

        list_string =', '.join(str(device) for device in devices)
        allowed_devices_list_string[group_name].set(list_string)

    if name.endswith("SP") or name.endswith("ENA"):
       
        autosave[name] = v

allowed_devices_pvs = {"ADMIN":{},"SISSY1":{}, "SISSY2":{}, "CAT":{}, "STXM":{}}
calc_pvs = {"ADMIN":{},"SISSY1":{}, "SISSY2":{}, "CAT":{}, "STXM":{}}
sp_pvs = {"ADMIN":{},"SISSY1":{}, "SISSY2":{}, "CAT":{}, "STXM":{}}

for group_name in allowed_devices_pvs.keys():

    for dev_name, item in dict_of_devices.items() :

        dev_name_edited = dev_name.upper().replace(".","_")
        if item == None:
            
            allowed_devices_pvs[group_name][dev_name] = builder.boolOut( group_name+ "_"+dev_name_edited + 'ENA', initial_value = False, ZNAM="Restore Disabled",ONAM="Restore Enabled",on_update_name=lambda v,name: update_allowed_devices_list(v,name))
            
        else:
            #there are positioners as components
            
            #create the top level device pv which will fanout to the others
            allowed_devices_pvs[group_name][dev_name] = builder.boolOut(group_name+ "_"+dev_name_edited + 'ENA', initial_value = False, ZNAM="Restore Disabled",ONAM="Restore Enabled",on_update_name=lambda v,name: update_allowed_devices_list(v,name))
            top_device_name = dev_name_edited
            
            for positioner_name in item:
                pos_name_edited = positioner_name.upper().replace(".","_")
                if "CHOICE" in pos_name_edited and ("M3" in dev_name_edited or "M4" in dev_name_edited):
                    #Then this is an SMU and we only want to move the choice parameter
                    allowed_devices_pvs[group_name][positioner_name] = builder.boolOut(group_name+ "_"+pos_name_edited + 'ENA', initial_value = False, ZNAM="Restore Disabled",ONAM="Restore Enabled",on_update_name=lambda v,name: update_allowed_devices_list(v,name))
                    
                elif positioner_name not in allowed_devices_pvs[group_name]:
                    sp_pvs[group_name][positioner_name] = builder.boolOut(group_name+ "_"+pos_name_edited + 'ENASP', initial_value = False, ZNAM="Restore Disabled",ONAM="Restore Enabled",on_update_name=lambda v,name: update_allowed_devices_list(v,name))
                    if group_name != 'ADMIN':
                        calc_pvs[group_name][positioner_name] = records.calcout(group_name+ "_"+pos_name_edited + 'ENACALC', CALC="A AND B AND C AND D",OUT=PREFIX +":"+group_name+ "_"+pos_name_edited + 'ENARB PP', OOPT="Every Time",INPA=builder.CP(allowed_devices_pvs[group_name][dev_name]), INPB=builder.CP(sp_pvs[group_name][positioner_name]),INPC=builder.CP(allowed_devices_pvs['ADMIN'][dev_name]), INPD=builder.CP(sp_pvs['ADMIN'][positioner_name]))
                    else:
                        calc_pvs[group_name][positioner_name] = records.calcout(group_name+ "_"+pos_name_edited + 'ENACALC', CALC="A AND B",OUT=PREFIX +":"+group_name+ "_"+pos_name_edited + 'ENARB PP', OOPT="Every Time",INPA=builder.CP(allowed_devices_pvs['ADMIN'][dev_name]), INPB=builder.CP(sp_pvs[group_name][positioner_name]))
                    allowed_devices_pvs[group_name][positioner_name] = builder.boolOut(group_name+ "_"+pos_name_edited + 'ENARB', initial_value = False, ZNAM="Restore Disabled",ONAM="Restore Enabled",on_update_name=lambda v,name: update_allowed_devices_list(v,name))


# Make a special PV that can be enabled and disabled for the PGM energy to be enabled and disabled for STXM

#stxm_pgm_energy = builder.aIn()
# Boilerplate get the IOC started
builder.LoadDatabase()
softioc.iocInit()

for pv_name in autosave:

    caput(pv_name, autosave[pv_name])
   

# Start processes required to be run after iocInit
def update():
    while True:
        #get the status
        command  ='status'
        method, params, monitoring_mode = create_msg([command],lock_key=None)
        

        #https://github.com/bluesky/bluesky-widgets/blob/master/bluesky_widgets/models/run_engine_client.py
        response = client.send_message(method=method)

        #get the env_exists

        env_exists.set(response['worker_environment_exists'])
        
        if response['worker_environment_state'] == "idle":
            re_running.set(0)
        elif response['worker_environment_state'] == "running":
            re_running.set(1)
        else:
            re_running.set(2) # some error

        # get the status of a save instruction:
        save_status.set(uid_status(save_uid.get()))
        restore_status.set(uid_status(restore_uid.get()))


        cothread.Sleep(1)

def update_autosave():

    while True:

        
        with open(file_path, 'w') as fp:
            json.dump( autosave,fp)

        cothread.Sleep(5)





cothread.Spawn(update)
cothread.Spawn(update_autosave)
# Restore autosave positions


# Finally leave the IOC running with an interactive shell.
softioc.interactive_ioc(globals())

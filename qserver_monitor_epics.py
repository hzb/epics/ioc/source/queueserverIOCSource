from softioc import softioc, builder,asyncio_dispatcher
import asyncio

from softioc.builder import records
# Create an asyncio dispatcher, the event loop is now running
dispatcher = asyncio_dispatcher.AsyncioDispatcher()
from collections import deque

# Set the record prefix
SYS = "EMILEL:QS:" 
DEV = "UE48"
PREFIX = SYS + DEV
builder.SetDeviceName(PREFIX)

console_monitor = builder.longStringIn('CONSOLE', initial_value = "",length=50000)

# Boilerplate get the IOC started
builder.LoadDatabase()
softioc.iocInit(dispatcher)

import sys
from bluesky_queueserver import ReceiveConsoleOutputAsync

message_buffer = deque([], 100)

rco = ReceiveConsoleOutputAsync(zmq_subscribe_addr='tcp://127.0.0.1:60625')

 # Start processes required to be run after iocInit
async def run_acquisition():
    while True:
        try:
            payload = await rco.recv()
            time, msg = payload.get("time", None), payload.get("msg", None)
            # In this example the messages are printed in the terminal.
           
            message_buffer.append(msg)
            console_monitor.set(''.join(message_buffer))
        except TimeoutError:
            # Timeout does not mean communication error!!!
            # Insert the code that needs to be executed on timeout (if any).
            pass
        # Place for the code that should be executed after receiving each
        #   message or after timeout (e.g. check a condition and exit
        #   the loop once the condition is satisfied).
        
rco.subscribe()
dispatcher(run_acquisition)
# Finally leave the IOC running with an interactive shell.
softioc.interactive_ioc(globals())

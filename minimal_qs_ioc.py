# Import the basic framework components.
# Import the basic framework components.
from cothread.catools import caget, connect, camonitor,caput
from softioc import softioc, builder
import cothread

#synchronous communication with the qserver
from bluesky_queueserver_api.zmq import REManagerAPI

from bluesky_queueserver.manager.qserver_cli import create_msg, zmq_single_request, QServerExitCodes
from bluesky_queueserver import ZMQCommSendThreads, CommTimeoutError, bind_plan_arguments, ReceiveConsoleOutput
from softioc.builder import records
import json

#Initialise Run Enging Manager Connection. Default is to localhost
RM = REManagerAPI()

address = 'tcp://127.0.0.1:60615'
client = ZMQCommSendThreads(zmq_server_address=address)